package outdoorcage;

import animal.Animal;

public class OutdoorCage {
    //Method Check Type untuk Wild Type Animal (Lion, Eagle)
    public static void checkType(Animal animal) {
        if (animal.getLength() < 75) {
            animal.setType("A");
        } else if (animal.getLength() < 90) {
            animal.setType("B");
        } else {
            animal.setType("C");
        }
    }
}
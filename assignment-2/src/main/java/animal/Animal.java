package animal;

public class Animal {

    protected String name;
    protected int length;
    protected String type;

    //Konstruktor untuk Animal dan subclassnya
    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
        this.type = "?";
    }

    //Setter Getter
    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
package lion;

import animal.Animal;

public class Lion extends Animal {
    public Lion(String name, int length) {
        super(name, length);
    }

    //Method Hunt Lion
    public void hunt() {
        System.out.println("Lion is hunting..\n"
                + this.name + " makes a voice: err...!");
    }

    //Method Brush Mane Lion
    public void brushMane() {
        System.out.println("Clean the lion's mane..\n"
                + this.name + " makes a voice: Hauhhmm!");
    }

    //Method Distrub Lion
    public void disturb() {
        System.out.println(this.name + " makes a voice: HAUHHMM!");
    }
}
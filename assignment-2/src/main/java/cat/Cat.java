package cat;

import animal.Animal;
import java.util.Random;

public class Cat extends Animal {
    public Cat(String name, int length) {
        super(name, length);
    }

    //Method brush fur untuk Cat
    public void brushFur() {
        System.out.println("Time to clean " + this.name + "'s fur\n" + this.name
                + " makes a voice: Nyaaan...");
    }

    //Method cuddle untuk Cat
    public void cuddle() {
        Random rand = new Random();
        String[] voice = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        int idx = rand.nextInt(4); //untuk merandom index di voice
        System.out.println(this.name + " makes a voice: " + voice[idx]);
    }
}

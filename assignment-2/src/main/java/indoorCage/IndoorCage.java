package indoorcage;

import animal.Animal;

public class IndoorCage {
    //Method Check Type untuk Pet Type Animal (Cat, Parrot, Hamster)
    public static void checkType(Animal animal) {
        if (animal.getLength() < 45) {
            animal.setType("A");
        } else if (animal.getLength() < 60) {
            animal.setType("B");
        } else {
            animal.setType("C");
        }
    }
}
package parrot;

import animal.Animal;

public class Parrot extends Animal {
    public Parrot(String name, int length) {
        super(name, length);
    }

    //Method Fly untuk Parrot
    public void fly() {
        System.out.println("Parrot " + this.name + " flies!\n"
                + this.name + " makes a voice: FLYYYY...");
    }

    //Method Conversation dengan Parrot
    public void conversation(String sentence) {
        System.out.println(this.name + " says: " + sentence.toUpperCase());
    }

    //Method mendiamkan Parrot
    public void alone() {
        System.out.println(this.name + " says: HM?");
    }
}
package arrangement;

import animal.Animal;
import cat.Cat;
import eagle.Eagle;
import hamster.Hamster;
import indoorcage.IndoorCage;
import java.util.ArrayList;
import lion.Lion;
import outdoorcage.OutdoorCage;
import parrot.Parrot;

public class Arrangement {
    //Mmethod untuk Menjalankan checkType di Indoor/Outdoor Cage
    public static void setType(ArrayList<? extends Animal> animals) {
        for (Animal animal : animals) {
            if (animal instanceof Cat | animal instanceof Parrot | animal instanceof Hamster) {
                IndoorCage.checkType(animal);
            } else {
                OutdoorCage.checkType(animal);
            }
        }
    }

    //Method Menentukan Level Cage
    public static String[][] assignLevel(ArrayList<? extends Animal> animals) {
        int size = animals.size();
        setType(animals);
        String[][] level = new String[3][size / 3 + 1];
        int num = 0; //Jumlah minimal hewan di 1 level
        int rem = 0; //Sisa hewan
        if (size <= 3) {
            num = 1;
            rem = size % 3 - 1;
        } else if (size > 3) {
            num = size / 3;
            rem = size % 3;
        }

        //Mengisi level 1
        for (int i = 0; i < num; i++) { //Untuk num hewan pertama
            level[0][i] = animals.get(i).getName() + " (" + animals.get(i).getLength()
                    + " - " + animals.get(i).getType() + "), ";
        }

        //Mengisi level 2 dan 3 untuk sisa hewan 1 atau size 3
        if (rem == 1 | size == 3) {
            int j = num;
            for (int i = 0; i < num; i++) { //untuk num hewan kedua
                level[1][i] = animals.get(j).getName() + " (" + animals.get(j).getLength()
                        + " - " + animals.get(j).getType() + "), ";
                j++;
            }
            j = num * 2;
            if (size == 3) {
                for (int i = 0; i < num; i++) { //untuk hewan terakhir (jumlah hewan 3)
                    level[2][i] = animals.get(j).getName() + " (" + animals.get(j).getLength()
                            + " - " + animals.get(j).getType() + "), ";
                    j++;
                }
            }
            if (size > 3) {
                for (int i = 0; i < num + 1; i++) { //untuk num+1 hewan ketiga
                    level[2][i] = animals.get(j).getName() + " (" + animals.get(j).getLength()
                            + " - " + animals.get(j).getType() + "), ";
                    j++;
                }
            }
        }

        //Mengisi level 2 dan 3 untuk sisa hewan 2
        if (rem == 2) {
            int j = num;
            for (int i = 0; i < num + 1; i++) { //untuk num+1 hewan kedua
                level[1][i] = animals.get(j).getName() + " (" + animals.get(j).getLength()
                        + " - " + animals.get(j).getType() + "), ";
                j++;
            }
            j = num * 2 + 1;
            for (int i = 0; i < num + 1; i++) { //untuk num+1 hewan ketiga
                level[2][i] = animals.get(j).getName() + " (" + animals.get(j).getLength()
                        + " - " + animals.get(j).getType() + "), ";
                j++;
            }
        }
        return level;
    }

    public static void arrange(ArrayList<? extends Animal> animals, String location) {
        if (animals.size() > 0) { //hanya akan mengarrange jika hewan ada
            String[][] level = assignLevel(animals);

            //Print Arrangement
            System.out.print("location: " + location + "\nlevel 3: "); //Llevel 3
            for (int i = 0; i < level[2].length; i++) {
                if (level[2][i] != null) {
                    System.out.print(level[2][i]);
                } else {
                    System.out.print("");
                }
            }
            System.out.print("\nlevel 2: "); //level 2
            for (int i = 0; i < level[1].length; i++) {
                if (level[1][i] != null) {
                    System.out.print(level[1][i]);
                } else {
                    System.out.print("");
                }
            }
            System.out.print("\nlevel 1: "); //;evel 1
            for (int i = 0; i < level[0].length; i++) {
                if (level[0][i] != null) {
                    System.out.print(level[0][i]);
                } else {
                    System.out.print("");
                }
            }
            System.out.println("\n");
            afterArrange(level);
        }
    }

    public static void afterArrange(String[][] level) {
        //Print After Arrangement
        System.out.print("After rearrangement...\nlevel 3: "); //level 2 jadi 3
        for (int i = level[1].length; i > 0; i--) {
            if (level[1][i - 1] != null) {
                System.out.print(level[1][i - 1]); //mereverse dalamnya
            } else {
                System.out.print("");
            }
        }
        System.out.print("\nlevel 2: "); //level 1 jadi 2
        for (int i = level[0].length; i > 0; i--) {
            if (level[0][i - 1] != null) {
                System.out.print(level[0][i - 1]); //mereverse dalamnya
            } else {
                System.out.print("");
            }
        }
        System.out.print("\nlevel 1: "); //level 3 jadi 1
        for (int i = level[2].length; i > 0; i--) {
            if (level[2][i - 1] != null) {
                System.out.print(level[2][i - 1]); //mereverse dalamnya
            } else {
                System.out.print("");
            }
        }
        System.out.println("\n");
    }
}
import animal.Animal;
import arrangement.Arrangement;
import cat.Cat;
import eagle.Eagle;
import hamster.Hamster;
import java.util.ArrayList;
import java.util.Scanner;
import lion.Lion;
import parrot.Parrot;

public class JavariPark {
    public static Animal checkName(String name, ArrayList<? extends Animal> animals) {
        for (Animal animal : animals) {
            if (animal.getName().equals(name)) {
                return animal;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Cat> cats = new ArrayList<Cat>();
        ArrayList<Eagle> eagles = new ArrayList<Eagle>();
        ArrayList<Hamster> hamsters = new ArrayList<Hamster>();
        ArrayList<Parrot> parrots = new ArrayList<Parrot>();
        ArrayList<Lion> lions = new ArrayList<Lion>();
        ArrayList<Animal> animalList = new ArrayList<Animal>();

        System.out.println("Welcome to Javari Park!\nInput the number of animals");
        String[] animals = {"cat", "lion", "eagle", "parrot", "hamster"};
        try {
            for (int i = 0; i < 5; i++) {
                System.out.print(animals[i] + ": ");
                int total = input.nextInt();
                input.nextLine();
                while (total < 0) {
                    System.out.println("The number of the animals can not be negative!");
                    System.out.print(animals[i] + ": ");
                    total = input.nextInt();
                    input.nextLine();
                }
                if (total > 0) {
                    System.out.println("Provide the information of " + animals[i] + "(s):");
                    String info = input.nextLine();
                    String info2 = info.replaceAll("\\s+", "");
                    String[] animal;
                    if (total > 1) {
                        animal = info2.split(",");
                        while (animal.length != total) {
                            System.out.println("Enter the information correctly");
                            info = input.nextLine();
                            info2 = info.replaceAll("\\s+", "");
                            animal = info2.split(",");
                        }
                    } else {
                        CharSequence coma = ",";
                        while (info2.contains(coma) | info2.length() == 0) {
                            System.out.println("Enter the information correctly!");
                            info = input.nextLine();
                            info2 = info.replaceAll("\\s+", "");
                        }
                        animal = new String[1];
                        animal[0] = info2;
                    }
                    for (int j = 0; j < total; j++) {
                        int idx = animal[j].indexOf("|");
                        String name = animal[j].substring(0, idx);
                        String lengthStr = animal[j].substring(idx + 1);
                        int lengthInt = Integer.parseInt(lengthStr);
                        if (i == 0) {
                            cats.add(new Cat(name, lengthInt));
                            animalList.add(new Cat(name, lengthInt));
                        } else if (i == 1) {
                            lions.add(new Lion(name, lengthInt));
                            animalList.add(new Lion(name, lengthInt));
                        } else if (i == 2) {
                            eagles.add(new Eagle(name, lengthInt));
                            animalList.add(new Eagle(name, lengthInt));
                        } else if (i == 3) {
                            parrots.add(new Parrot(name, lengthInt));
                            animalList.add(new Parrot(name, lengthInt));
                        } else if (i == 4) {
                            hamsters.add(new Hamster(name, lengthInt));
                            animalList.add(new Hamster(name, lengthInt));
                        }
                    }
                }
            }
            System.out.println("Animals have been successfully recorded!\n\n"
                    + "=============================================\nCage arrangement:");
            Arrangement.arrange(cats, "indoor");
            Arrangement.arrange(lions, "outdoor");
            Arrangement.arrange(eagles, "outdoor");
            Arrangement.arrange(parrots, "indoor");
            Arrangement.arrange(hamsters, "indoor");
            System.out.println("NUMBER OF ANIMALS:\ncat:" + cats.size()
                    + "\nlion:" + lions.size() + "\nparrot:" + parrots.size()
                    + "\neagle:" + eagles.size() + "\nhamster:" + hamsters.size()
                    + "\n\n=============================================");
        } catch (Exception e) {
            System.out.println("Your input is invalid!\n");
            main(null);
        }

        while (true) {
            try {
                System.out.println("Which animal you want to visit?\n"
                        + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
                String[] animal = {"cat", "eagle", "hamster", "parrot", "lion"};
                int visit = input.nextInt();
                input.nextLine();
                if (visit == 99) {
                    System.exit(0);
                }
                System.out.print("Mention the name of " + animal[visit - 1]
                        + " you want to visit: ");
                String name = input.nextLine();
                if (visit == 1) {
                    if (checkName(name, cats) != null) {
                        System.out.println("You are visiting " + name + " (cat) now, "
                                + "what would you like to do?\n1: Brush the fur 2: Cuddle");
                        int toDo = input.nextInt();
                        Cat cat = (Cat) checkName(name, cats);
                        if (toDo == 1) {
                            cat.brushFur();
                        } else if (toDo == 2) {
                            cat.cuddle();
                        } else {
                            System.out.println("You do nothing...");
                        }
                    } else if (checkName(name, cats) == null) {
                        System.out.print("There is no cat with that name! ");
                    }
                } else if (visit == 2) {
                    if (checkName(name, eagles) != null) {
                        System.out.println("You are visiting " + name + " (eagle) now, "
                                + "what would you like to do?\n1: Order to fly");
                        int toDo = input.nextInt();
                        Eagle eagle = (Eagle) checkName(name, eagles);
                        if (toDo == 1) {
                            eagle.flyHigh();
                        } else {
                            System.out.println("You do nothing...");
                        }
                    } else if (checkName(name, eagles) == null) {
                        System.out.print("There is no eagle with that name! ");
                    }
                } else if (visit == 3) {
                    if (checkName(name, hamsters) != null) {
                        System.out.println("You are visiting " + name + " (hamster) now, "
                                + "what would you like to do?\n1: See it gnawing "
                                + "2: Order to run in the hamster wheel");
                        int toDo = input.nextInt();
                        Hamster hamster = (Hamster) checkName(name, hamsters);
                        if (toDo == 1) {
                            hamster.gnaw();
                        } else if (toDo == 2) {
                            hamster.wheelRun();
                        } else {
                            System.out.println("You do nothing...");
                        }
                    } else if (checkName(name, hamsters) == null) {
                        System.out.print("There is no hamster with that name! ");
                    }
                } else if (visit == 4) {
                    if (checkName(name, parrots) != null) {
                        System.out.println("You are visiting " + name + " (parrot) now, "
                                + "what would you like to do?\n1: Order to fly 2: Do conversation");
                        int toDo = input.nextInt();
                        Parrot parrot = (Parrot) checkName(name, parrots);
                        if (toDo == 1) {
                            parrot.fly();
                        } else if (toDo == 2) {
                            System.out.print("You say: ");
                            input.nextLine();
                            String say = input.nextLine();
                            parrot.conversation(say);
                        } else {
                            parrot.alone();
                        }
                    } else if (checkName(name, parrots) == null) {
                        System.out.print("There is no parrot with that name! ");
                    }
                } else if (visit == 5) {
                    if (checkName(name, lions) != null) {
                        System.out.println("You are visiting " + name + " (lion) now, "
                                + "what would you like to do?\n1: See it hunting "
                                + "2: Brush the mane 3: Disturb it");
                        int toDo = input.nextInt();
                        Lion lion = (Lion) checkName(name, lions);
                        if (toDo == 1) {
                            lion.hunt();
                        } else if (toDo == 2) {
                            lion.brushMane();
                        } else {
                            lion.disturb();
                        }
                    } else if (checkName(name, lions) == null) {
                        System.out.print("There is no lions with that name! ");
                    }
                }
                System.out.println("Back to the office!\n");
            } catch (Exception e) {
                System.out.println("Your input is invalid!\n");
            }
        }
    }
}
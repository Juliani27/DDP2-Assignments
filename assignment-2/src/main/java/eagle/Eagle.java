package eagle;

import animal.Animal;

public class Eagle extends Animal {
    public Eagle(String name, int length) {
        super(name, length);
    }

    //Method Fly High untuk Eagle
    public void flyHigh() {
        System.out.println(this.name + " makes a voice: kwaakk...\nYou hurt!");
    }
}
package hamster;

import animal.Animal;

public class Hamster extends Animal {
    public Hamster(String name, int length) {
        super(name, length);
    }

    //Method Gnaw untuk Hamster
    public void gnaw() {
        System.out.println(this.name + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    //Method Run in the Hamster Wheel untuk Hamster
    public void wheelRun() {
        System.out.println(this.name + " makes a voice: trrr... trrr...");
    }
}
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.Registration;
import javari.park.SelectedAttraction;
import javari.park.Visitor;
import javari.reader.CsvSections;
import javari.writer.RegistrationWriter;

public class A3Festival {
    private static Visitor visitor;
    private static int visitorId = 1;

    //Fungsi untuk mengecek apakah string dapat djadikan angka
    private static boolean isNumber(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        //Proses CSV
        Scanner input = new Scanner(System.in);
        System.out.print("Welcome to Javari Park Festival - Registration Service!\n\n"
            + "... Opening default section database from data.");
        //mengecek di folder yang sama dengan file A3Festival
        if (CsvProcess.searchFile("C:\\Users\\Asus\\Documents\\Juliani\\CSUI\\Matkul\\DDP2\\"
            + "ProgFound2-Assignments\\assignment-3\\src\\main\\java")) {
            System.out.println("... Loading... Success... System is populating data...\n");
            //Jika tidak ada, meminta input path direktori
        } else {
            System.out.print("... File not found or incorrect file!\n"
                + "\nPlease provide the source data path: ");
            String dirName = input.nextLine();
            if (CsvProcess.searchFile(dirName)) {
                System.out.println("\n... Loading... Success... System is populating data...\n");
            }
            while (!CsvProcess.searchFile(dirName)) {
                System.out.print("\n... File not found or incorrect file!\n"
                    + "\nPlease provide the source data path: ");
                dirName = input.nextLine();
                if (CsvProcess.searchFile(dirName)) {
                    System.out.println("\n... Loading... Success... System is populating data..\n");
                    break;
                }
            }
        }
        CsvProcess.csvRead();
        while (true) {
            System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n\n"
                + "Please answer the questions by typing the number."
                + "Type # if you want to return to the previous menu");
            boolean sectionWhile = true;
            while (sectionWhile) {
                List<String> validSections = CsvSections.getValidSections();
                RegistrationMenu.sections(validSections);
                String section = input.nextLine();
                if (section.equals("#")) {
                    continue;
                }
                while (!isNumber(section) || Integer.parseInt(section) < 1
                    || Integer.parseInt(section) > validSections.size()) {
                    System.out.println("\nYour input is invalid!");
                    RegistrationMenu.sections(validSections);
                    section = input.nextLine();
                }
                boolean typeWhile = true;
                while (typeWhile) {
                    int intSection = Integer.parseInt(section);
                    String kingdom = RegistrationMenu.kingdoms(validSections.get(intSection - 1));
                    String typeNum = input.nextLine();
                    if (typeNum.equals("#")) {
                        break;
                    }
                    while (!isNumber(typeNum)
                        || !RegistrationMenu.checkType(kingdom, Integer.parseInt(typeNum))) {
                        System.out.println("\nYour input is invalid!");
                        kingdom = RegistrationMenu.kingdoms(section);
                        typeNum = input.nextLine();
                    }
                    boolean attrWhile = true;
                    while (attrWhile) {
                        List<Attraction> selectedAttr = RegistrationMenu.attraction(kingdom,
                            Integer.parseInt(typeNum));
                        if (selectedAttr == null) {
                            break;
                        }
                        String attrStr = input.nextLine();
                        if (attrStr.equals("#")) {
                            break;
                        }
                        while (!isNumber(attrStr) || Integer.parseInt(attrStr) < 1
                            || Integer.parseInt(attrStr) > selectedAttr.size()) {
                            System.out.println("\nYour input is invalid!");
                            selectedAttr = RegistrationMenu.attraction(kingdom,
                                Integer.parseInt(typeNum));
                            attrStr = input.nextLine();
                        }
                        int attrNum = Integer.parseInt(attrStr);
                        String name;
                        if (visitor == null) {
                            System.out.println("\nWow, one more step,"
                                + "\nplease let us know your name: ");
                            name = input.nextLine();
                            visitor = new Visitor(name, visitorId);
                        } else {
                            name = visitor.getVisitorName();
                        }
                        visitor.addSelectedAttraction(selectedAttr.get(attrNum - 1));
                        System.out.print("\nYeay, final check!"
                            + "\nHere is your data, and the attraction you chose:"
                            + "\nName: " + name + "\nAttractions: ");
                        String attractions = "";
                        for (SelectedAttraction attr : visitor.getSelectedAttractions()) {
                            String animalType = Character.toUpperCase(attr.getType().charAt(0))
                                + attr.getType().substring(1);
                            if (visitor.getSelectedAttractions().size() == 1) {
                                attractions += (attr.getName() + " -> " + animalType);
                                System.out.print(attractions);
                                attractions = "  ";
                            } else {
                                attractions += (attr.getName() + " -> " + animalType + ", ");
                            }
                        }
                        System.out.print(attractions.substring(0, attractions.length() - 2)
                            + "\nWith: ");
                        for (SelectedAttraction attr : visitor.getSelectedAttractions()) {
                            List<String> performers = new ArrayList<>();
                            for (Animal animal : attr.getPerformers()) {
                                if (animal.isShowable()) {
                                    performers.add(animal.getName());
                                }
                            }
                            if (visitor.getSelectedAttractions().size() == 1) {
                                System.out.print(performers);
                            } else {
                                if (visitor.getSelectedAttractions().indexOf(attr)
                                    != visitor.getSelectedAttractions().size() - 1) {
                                    System.out.print(performers + ", ");
                                } else {
                                    System.out.print(performers);
                                }

                            }
                        }
                        System.out.print("\n\nIs the data correct? (Y/N): ");
                        String correct = input.nextLine();
                        while (!correct.equalsIgnoreCase("Y") && !correct.equalsIgnoreCase("N")) {
                            System.out.print("\nYour input is invalid!"
                                + "\nIs the data correct? (Y/N): ");
                            correct = input.nextLine();
                        }
                        if (correct.equalsIgnoreCase("N")) {
                            visitor = null;
                        } else if (correct.equalsIgnoreCase("Y")) {
                            System.out.print("\nThank you for your interest."
                                + "Would you like to register to other attractions? (Y/N): ");
                            String other = input.nextLine();
                            while (!other.equalsIgnoreCase("Y") && !other.equalsIgnoreCase("N")) {
                                System.out.print("\nYour input is invalid!"
                                    + "\nWould you like to register to other attractions? (Y/N): ");
                                other = input.nextLine();
                            }
                            if (other.equalsIgnoreCase("N")) {
                                Path dir = Paths.get("C:\\Users\\Asus\\Documents\\Juliani\\CSUI"
                                    + "\\Matkul\\DDP2\\ProgFound2-Assignments\\assignment-3\\data");
                                try {
                                    RegistrationWriter.writeJson((Registration) visitor, dir);
                                } catch (IOException e) {
                                    System.out.println("Failed to write JSON registration file for "
                                        + visitor.getVisitorName());
                                }
                                visitor = null;
                                visitorId += 1;
                                attrWhile = false;
                                typeWhile = false;
                                sectionWhile = false;
                            }
                        }
                    }
                }
            }
        }
    }
}
import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;
import javari.park.Attraction;

public class RegistrationMenu {
    //Method untuk print section saat menu registrasi
    public static void sections(List<String> validSections) {
        System.out.println("\nJavari Park has " + validSections.size() + " sections:");
        for (int i = 0; i < validSections.size(); i++) {
            System.out.println((i + 1) + ". " + validSections.get(i));
        }
        System.out.print("Please choose your preferred section (type the number): ");
    }

    //Method untuk print hewan-hewan di section yang dipilih dan mereturn kategori sectionnya
    public static String kingdoms(String section) {
        switch (section) {
            case "Explore the Mammals":
                System.out.print("\n--Explore the Mammals--\n1. Hamster\n2. Lion\n3. Cat\n4. Whale"
                    + "\nPlease choose your preferred animals (type the number): ");
                return "Mammals";
            case "World of Aves":
                System.out.print("\n--World of Aves--\n1. Eagle\n2. Parrot"
                    + "\nPlease choose your preferred animals (type the number): ");
                return "Aves";
            default:
                System.out.print("\n--Reptilian Kingdom--\n1. Snake"
                    + "\nPlease choose your preferred animals (type the number): ");
                return "Reptilian";
        }
    }

    //Fungsi print atraksi oleh suatu hewan yang dipilih dan mereturn list atraksi hewan tersebut
    public static List<Attraction> attraction(String kingdom, int typeNum) {
        String[] types;
        List<Attraction> res = new ArrayList<>();
        switch (kingdom) {
            case "Mammals":
                types = new String[]{"Hamster", "Lion", "Cat", "Whale"};
                break;
            case "Aves":
                types = new String[]{"Eagle", "Parrot"};
                break;
            default:
                types = new String[]{"Snake"};
                break;
        }
        String type = types[typeNum - 1];
        if (checkAnimals(type.toLowerCase())) {
            System.out.println("\n---" + type + "---\n" + "Attractions by " + type + ":");
            int i = 1;
            for (Attraction attr : CsvProcess.allAttraction) {
                if (attr.getType().equalsIgnoreCase(types[typeNum - 1])) {
                    System.out.println(i + ". " + attr.getName());
                    i++;
                    res.add(attr);
                }
            }
            System.out.print("Please choose your preferred attractions (type the number): ");
            return res;
        } else {
            System.out.println("\nUnfortunately, no " + type
                + " can perform any attraction, please choose other animals");
            return null;
        }
    }

    //Fungsi untuk mengecek angka masukan valid atau tidak untuk kategori yang dipilih
    public static boolean checkType(String kingdom, int typeNum) {
        String[] types;
        switch (kingdom) {
            case "Mammals":
                types = new String[]{"Hamster", "Lion", "Cat", "Whale"};
                break;
            case "Aves":
                types = new String[]{"Eagle", "Parrot"};
                break;
            default:
                types = new String[]{"Snake"};
                break;
        }
        return typeNum <= types.length && typeNum >= 1;
    }

    //Fungsi untuk mengecek apakah tipe hewan ada atau tidak di list hewan
    private static boolean checkAnimals(String type) {
        for (Animal animal : CsvProcess.allAnimals) {
            if (animal.getType().equals(type) && animal.isShowable()) {
                return true;
            }
        }
        return false;
    }
}
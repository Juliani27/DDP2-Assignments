import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Mammals;
import javari.animal.Reptilian;
import javari.park.Attraction;
import javari.reader.CsvAttractions;
import javari.reader.CsvCategories;
import javari.reader.CsvRecords;
import javari.reader.CsvSections;

public class CsvProcess {
    public static ArrayList<Animal> allAnimals = new ArrayList<>();
    public static ArrayList<Attraction> allAttraction = new ArrayList<>();
    private static Path[] csvPath = new Path[3];

    //Fungsi untuk mencari apakah 3 file csv ada di direktori yang dipilih
    public static boolean searchFile(String dirName) {
        String[] files = {"animals_categories.csv", "animals_attractions.csv",
            "animals_records.csv"};
        dirName += "/";
        for (int i = 0; i < 3; i++) {
            File file = new File(dirName + files[i]);
            if (file.exists()) {
                csvPath[i] = Paths.get(dirName + files[i]);
            } else {
                return false;
            }
        }
        return true;
    }

    //Fungsi untuk membaca csv (memanggil method menghitung valid dan invalid)
    public static void csvRead() {
        try {
            CsvCategories csvCategories = new CsvCategories(csvPath[0]);
            CsvSections csvSections = new CsvSections(csvPath[0]);
            CsvAttractions csvAttractions = new CsvAttractions(csvPath[1]);
            CsvRecords csvRecords = new CsvRecords(csvPath[2]);
            System.out.println("Found _" + csvSections.countValidRecords()
                + "_ valid sections and _" + csvSections.countInvalidRecords()
                + "_ invalid sections");
            System.out.println("Found _" + csvCategories.countValidRecords()
                + "_ valid animal categories and _" + csvCategories.countInvalidRecords()
                + "_ invalid animal categories");
            csvAttractions.checkAnimals(csvCategories.getValidAnimals(), 
                csvSections.getValidAnimals());
            System.out.println("Found _" + csvAttractions.countValidRecords()
                + "_ valid attractions and _" + csvAttractions.countInvalidRecords()
                + "_ invalid attractions");
            csvRecords.checkAnimals(csvCategories.getValidAnimals(), csvSections.getValidAnimals());
            System.out.println("Found _" + csvRecords.countValidRecords()
                + "_ valid animal records and _" + csvRecords.countInvalidRecords()
                + "_ invalid animal records");
            addAnimal(csvRecords.getValidRecords());
            addAttractions(csvAttractions.getValidAttractions());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Fungsi untuk meng-instansiasi animal dan menambahkannya ke list animal
    private static void addAnimal(List<String[]> animals) {
        for (String[] info : animals) {
            int id = Integer.parseInt(info[0]);
            String type = info[1];
            String name = info[2];
            String gender = info[3];
            double length = Double.parseDouble(info[4]);
            double weight = Double.parseDouble(info[5]);
            String special = info[6];
            String condition = info[7];
            boolean status = true;
            if (condition.equals("nothealthy")) {
                condition = "not healthy";
            }
            if (special.equals("pregnant") || special.equals("wild") || special.equals("layeggs")) {
                status = false;
            }
            if (type.equals("lion") || type.equals("whale")
                || type.equals("cat") || type.equals("hamster")) {
                allAnimals.add(new Mammals(id, type, name, Gender.parseGender(gender),
                    length, weight, status, Condition.parseCondition(condition)));
            } else if (type.equals("parrot") || type.equals("eagle")) {
                allAnimals.add(new Aves(id, type, name, Gender.parseGender(gender),
                    length, weight, status, Condition.parseCondition(condition)));
            } else if (type.equals("snake")) {
                allAnimals.add(new Reptilian(id, type, name, Gender.parseGender(gender),
                    length, weight, status, Condition.parseCondition(condition)));
            }
        }
    }

    //Fungsi untuk meng-instansiasi atraksi dan menambahkannya ke list atraksi
    private static void addAttractions(List<String[]> attraction) {
        for (String[] info : attraction) {
            String name = info[0];
            String type = info[1];
            Attraction temp = new Attraction(name, type);
            allAttraction.add(temp);
            for (Animal animal : allAnimals) {
                if (type.equals(animal.getType())) {
                    temp.addPerformer(animal);
                }
            }
        }
    }

}
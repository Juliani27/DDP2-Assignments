package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class CsvCategories extends CsvReader {

    private List<String> validAnimals;

    public CsvCategories(Path file) throws IOException {
        super(file);
        this.validAnimals = new ArrayList<>();
    }

    //Fungsi untuk mereturn animal yang kategorinya valid
    public List<String> getValidAnimals() {
        return this.validAnimals;
    }

    //Fungsi untuk menghitung kategori animal yang valid
    public long countValidRecords() {
        int validMammals = 0;
        int validAves = 0;
        int validReptiles = 0;
        for (int i = 0; i < this.lines.size(); i++) {
            String[] info = this.lines.get(i).split(",");
            String animal = info[0].toLowerCase().replaceAll("\\s+", "");
            String type = info[1].toLowerCase().replaceAll("\\s+", "");
            if (type.equals("mammals")) {
                if (animal.equals("lion") || animal.equals("whale")
                    || animal.equals("cat") || animal.equals("hamster")) {
                    this.validAnimals.add(animal);
                    if (validMammals == 0) {
                        validMammals = 1;
                    }
                }
            } else if (type.equals("aves")) {
                if (animal.equals("parrot") || animal.equals("eagle")) {
                    this.validAnimals.add(animal);
                    if (validAves == 0) {
                        validAves = 1;
                    }
                }
            } else if (type.equals("reptiles") && animal.equals("snake")) {
                this.validAnimals.add(animal);
                if (validReptiles == 0) {
                    validReptiles = 1;
                }
            }
        }
        return validMammals + validAves + validReptiles;
    }

    //Fungsi untuk menghitung kategori yang invalid
    public long countInvalidRecords() {
        int invalidMammals = 0;
        int invalidAves = 0;
        int invalidReptiles = 0;
        int invalidOthers = 0;
        for (int i = 0; i < this.lines.size(); i++) {
            String[] info = this.lines.get(i).split(",");
            String animal = info[0].toLowerCase().replaceAll("\\s+", "");
            String type = info[1].toLowerCase().replaceAll("\\s+", "");
            if (type.equals("mammals")) {
                if (!(animal.equals("lion") || animal.equals("whale")
                    || animal.equals("cat") || animal.equals("hamster"))) {
                    if (invalidMammals == 0) {
                        invalidMammals = 1;
                    }
                }
            } else if (type.equals("aves")) {
                if (!(animal.equals("parrot") || animal.equals("eagle"))) {
                    if (invalidAves == 0) {
                        invalidAves = 1;
                    }
                }
            } else if (type.equals("reptiles")) {
                if (!(animal.equals("snake"))) {
                    if (invalidReptiles == 0) {
                        invalidReptiles = 1;
                    }
                }
            } else {
                invalidOthers += 1;
            }
        }
        return invalidMammals + invalidAves + invalidReptiles + invalidOthers;
    }
}
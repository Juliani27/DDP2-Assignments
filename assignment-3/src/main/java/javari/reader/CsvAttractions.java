package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class CsvAttractions extends CsvReader {
    private List<String> validAnimals;
    private List<String[]> validAttractions;

    public CsvAttractions(Path file) throws IOException {
        super(file);
        this.validAttractions = new ArrayList<>();
    }

    //Fungsi mereturn atraksi yang valid
    public List<String[]> getValidAttractions() {
        return this.validAttractions;
    }

    //Fungsi menghitung atraksi yang valid
    public long countValidRecords() {
        int validCircle = 0;
        int validDancing = 0;
        int validCounting = 0;
        int validCoders = 0;
        for (int i = 0; i < this.lines.size(); i++) {
            String[] info = this.lines.get(i).split(",");
            String type = info[0].toLowerCase().replaceAll("\\s+", "");
            String attraction = info[1].toLowerCase().replaceAll("\\s+", "");
            if (this.validAnimals.contains(type)) {
                if (attraction.equals("circlesoffires")) {
                    if (type.equals("lion") || type.equals("whale") || type.equals("eagle")) {
                        this.validAttractions.add(new String[]{"Circles of Fires", type});
                        if (validCircle == 0) {
                            validCircle = 1;
                        }
                    }
                } else if (attraction.equals("dancinganimals")) {
                    if (type.equals("parrot") || type.equals("snake") || type.equals("cat")
                        || type.equals("hamster")) {
                        this.validAttractions.add(new String[]{"Dancing Animals", type});
                        if (validDancing == 0) {
                            validDancing = 1;
                        }
                    }
                } else if (attraction.equals("countingmasters")) {
                    if (type.equals("hamster") || type.equals("whale") || type.equals("parrot")) {
                        this.validAttractions.add(new String[]{"Counting Masters", type});
                        if (validCounting == 0) {
                            validCounting = 1;
                        }
                    }
                } else if (attraction.equals("passionatecoders")) {
                    if (type.equals("hamster") || type.equals("cat") || type.equals("snake")) {
                        this.validAttractions.add(new String[]{"Passionate Coders", type});
                        if (validCoders == 0) {
                            validCoders = 1;
                        }
                    }
                }
            }
        }
        return validCircle + validDancing + validCounting + validCoders;
    }

    //Fungsi menghitung atraksi yang tidak valid
    public long countInvalidRecords() {
        int invalidCircle = 0;
        int invalidDancing = 0;
        int invalidCounting = 0;
        int invalidCoders = 0;
        int invalidOthers = 0;
        for (int i = 0; i < this.lines.size(); i++) {
            String[] info = this.lines.get(i).split(",");
            String type = info[0].toLowerCase().replaceAll("\\s+", "");
            String attraction = info[1].toLowerCase().replaceAll("\\s+", "");
            if (!this.validAnimals.contains(type)) {
                invalidOthers += 1;
            } else if (attraction.equals("circlesoffires")) {
                if (!(type.equals("lion") || type.equals("whale") || type.equals("eagle"))) {
                    if (invalidCircle == 0) {
                        invalidCircle = 1;
                    }
                }
            } else if (attraction.equals("dancinganimals")) {
                if (!(type.equals("parrot") || type.equals("snake") || type.equals("cat")
                    || type.equals("hamster"))) {
                    if (invalidDancing == 0) {
                        invalidDancing = 1;
                    }
                }
            } else if (attraction.equals("countingmasters")) {
                if (!(type.equals("hamster") || type.equals("whale") || type.equals("parrot"))) {
                    if (invalidCounting == 0) {
                        invalidCounting = 1;
                    }
                }
            } else if (attraction.equals("passionatecoders")) {
                if (!(type.equals("hamster") || type.equals("cat") || type.equals("snake"))) {
                    if (invalidCoders == 0) {
                        invalidCoders = 1;
                    }
                }
            } else {
                invalidOthers += 1;
            }
        }
        return invalidCircle + invalidDancing + invalidCounting + invalidCoders + invalidOthers;
    }

    //Fungsi untuk mendapatkan animal yang valid setelah dicek section dan kategori
    public void checkAnimals(List<String> animalCategory, List<String> animalSection) {
        this.validAnimals = new ArrayList<>(animalCategory);
        this.validAnimals.retainAll(animalSection);
    }
}

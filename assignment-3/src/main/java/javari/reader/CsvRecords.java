package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class CsvRecords extends CsvReader {
    private int invalid;
    private List<String> validAnimals;
    private List<String[]> validRecords;

    public CsvRecords(Path file) throws IOException {
        super(file);
        this.invalid = 0;
        this.validRecords = new ArrayList<>();
    }

    //Fungsi mengembalikan data animal yang valid
    public List<String[]> getValidRecords() {
        return this.validRecords;
    }

    //Fungsi untuk mengecek apakah string bisa dijadikan double
    private boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    //Fungsi untuk mengecek apakah string bisa dijadikan int
    private boolean isInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    //Fungsi untuk menghitung records yang valid
    public long countValidRecords() {
        List<String> ids = new ArrayList<>();
        int valid = 0;
        for (int i = 0; i < this.lines.size(); i++) {
            String[] info = this.lines.get(i).split(",");
            String id = info[0].replaceAll("\\s+", "");
            String type = info[1].toLowerCase().replaceAll("\\s+", "");
            String name = info[2].replaceAll("\\s+", "");
            String gender = info[3].toLowerCase().replaceAll("\\s+", "");
            String length = info[4].replaceAll("\\s+", "");
            String weight = info[5].replaceAll("\\s+", "");
            String special = info[6].toLowerCase().replaceAll("\\s+", "");
            String condition = info[7].toLowerCase().replaceAll("\\s+", "");
            if (this.validAnimals.contains(type) && !ids.contains(id) && isInt(id)) {
                if (gender.equals("male") || gender.equals("female")) {
                    if (isDouble(length) && isDouble(weight)) {
                        if (condition.equals("healthy") || condition.equals("nothealthy")) {
                            valid += 1;
                            ids.add(id);
                            this.validRecords.add(new String[]{id,
                                type, name, gender, length, weight, special, condition});
                        } else {
                            this.invalid += 1;
                        }
                    } else {
                        this.invalid += 1;
                    }
                } else {
                    this.invalid += 1;
                }
            } else {
                this.invalid += 1;
            }
        }
        return valid;
    }

    //Fungsi untuk mereturn records yang invalid
    public long countInvalidRecords() {
        return this.invalid;
    }

    //Fungsi untuk mendapatkan animal yang valid setelah dicek section dan kategori
    public void checkAnimals(List<String> animalCategory, List<String> animalSection) {
        this.validAnimals = new ArrayList<>(animalCategory);
        this.validAnimals.retainAll(animalSection);
    }
}

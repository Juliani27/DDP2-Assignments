package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class CsvSections extends CsvReader {
    private static List<String> validSections = new ArrayList<>();
    private List<String> validAnimals;

    public CsvSections(Path file) throws IOException {
        super(file);
        this.validAnimals = new ArrayList<>();
    }

    //Fungsi untuk mereturn section yang valid
    public static List<String> getValidSections() {
        return validSections;
    }

    //Fungsi untuk mereturn animal yang sectionnya valid
    public List<String> getValidAnimals() {
        return this.validAnimals;
    }

    //Fungsi untuk menghitung section yang valid
    public long countValidRecords() {
        int validMammals = 0;
        int validAves = 0;
        int validReptiles = 0;
        for (int i = 0; i < this.lines.size(); i++) {
            String[] info = this.lines.get(i).split(",");
            String animal = info[0].toLowerCase().replaceAll("\\s+", "");
            String kingdom = info[2].toLowerCase().replaceAll("\\s+", "");
            if (kingdom.equals("explorethemammals")) {
                if (animal.equals("lion") || animal.equals("whale")
                    || animal.equals("cat") || animal.equals("hamster")) {
                    this.validAnimals.add(animal);
                    if (validMammals == 0) {
                        validMammals = 1;
                        validSections.add("Explore the Mammals");
                    }
                }
            } else if (kingdom.equals("worldofaves")) {
                if (animal.equals("parrot") || animal.equals("eagle")) {
                    this.validAnimals.add(animal);
                    if (validAves == 0) {
                        validAves = 1;
                        validSections.add("World of Aves");
                    }
                }
            } else if (kingdom.equals("reptiliankingdom") && animal.equals("snake")) {
                this.validAnimals.add(animal);
                if (validReptiles == 0) {
                    validReptiles = 1;
                    validSections.add("Reptilian Kingdom");
                }
            }
        }
        return validMammals + validAves + validReptiles;
    }

    //Fungsi untuk menghitung section yang valid
    public long countInvalidRecords() {
        int invalidMammals = 0;
        int invalidAves = 0;
        int invalidReptiles = 0;
        int invalidOthers = 0;
        for (int i = 0; i < this.lines.size(); i++) {
            String[] info = this.lines.get(i).split(",");
            String animal = info[0].toLowerCase().replaceAll("\\s+", "");
            String kingdom = info[2].toLowerCase().replaceAll("\\s+", "");
            if (kingdom.equals("explorethemammals")) {
                if (!(animal.equals("lion") || animal.equals("whale")
                    || animal.equals("cat") || animal.equals("hamster"))) {
                    if (invalidMammals == 0) {
                        invalidMammals = 1;
                    }
                }
            } else if (kingdom.equals("worldofaves")) {
                if (!(animal.equals("parrot") || animal.equals("eagle"))) {
                    if (invalidAves == 0) {
                        invalidAves = 1;
                    }
                }
            } else if (kingdom.equals("reptiliankingdom")) {
                if (!(animal.equals("snake"))) {
                    if (invalidReptiles == 0) {
                        invalidReptiles = 1;
                    }
                }
            } else {
                invalidOthers += 1;
            }
        }
        return invalidMammals + invalidAves + invalidReptiles + invalidOthers;
    }
}

package javari.animal;

public class Aves extends Animal {
    private boolean layEggs;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, boolean specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.layEggs = specialStatus;
    }

    //Specific condition untuk aves
    public boolean specificCondition() {
        return !this.layEggs; //hanya aves yang tidak sedang bertelur yang bisa atraksi
    }
}
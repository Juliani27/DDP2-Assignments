package javari.animal;

public class Mammals extends Animal {
    private boolean pregnant;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight, boolean specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.pregnant = specialStatus;
    }

    //Specific condition untuk mammals
    public boolean specificCondition() {
        if (this.getType().equals("Lion")) { //untuk singa, hanya singa jantan yang bisa atraksi
            return this.getGender() == Gender.MALE;
        } else {
            return !this.pregnant; //hanya mammals yang tidak hamil yang bisa atraksi
        }
    }
}
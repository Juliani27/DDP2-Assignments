package javari.animal;

public class Reptilian extends Animal {
    private boolean tame;

    public Reptilian(Integer id, String type, String name, Gender gender, double length,
                     double weight, boolean specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.tame = specialStatus;
    }

    //Specific condition untuk reptil
    public boolean specificCondition() {
        return this.tame; //hanya reptil jinak yang bisa atraksi
    }
}
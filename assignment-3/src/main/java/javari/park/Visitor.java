package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {
    private String name;
    private int id;
    private List<SelectedAttraction> attractions;

    public Visitor(String name, int id) {
        this.name = name;
        this.id = id;
        this.attractions = new ArrayList<>();
    }

    public int getRegistrationId() {
        return this.id;
    }

    public String getVisitorName() {
        return this.name;
    }

    public String setVisitorName(String name) {
        this.name = name;
        return "";
    }

    public List<SelectedAttraction> getSelectedAttractions() {
        return this.attractions;
    }

    //Fungsi untuk menambah atraksi visitor
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.attractions.add(selected);
        return true;
    }
}
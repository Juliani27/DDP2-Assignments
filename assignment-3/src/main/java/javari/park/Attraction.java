package javari.park;

import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;

public class Attraction implements SelectedAttraction {
    private String name;
    private String type;
    private List<Animal> performers;

    public Attraction(String name, String type) {
        this.name = name;
        this.type = type;
        this.performers = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public List<Animal> getPerformers() {
        return this.performers;
    }

    //Fungsi menambah performer ke suatu atraksi
    public boolean addPerformer(Animal performer) {
        this.performers.add(performer);
        return true;
    }
}

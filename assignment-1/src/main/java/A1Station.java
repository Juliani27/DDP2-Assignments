import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    /* Method menghitung banyak TrainCar untuk diberangkatkan */
    public static int trainToDepart(TrainCar[] trainCar, int num, int total) {
        if (trainCar[num].computeTotalWeight() > THRESHOLD) {
            return (num);
        } else if (num == total) {
            return num;
        } else {
            return trainToDepart(trainCar, num + 1, total);
        }
    }

    /* Method memberangkatkan Train */
    public static void depart(TrainCar[] trainCar, int num, int total) {
        String categori = "";
        double average = trainCar[num].computeTotalMassIndex() / (total);
        if (average < 18.5) {
            categori = "underweight";
        } else if (average < 25) {
            categori = "normal";
        } else if (average < 30) {
            categori = "overweight";
        } else if (average >= 30) {
            categori = "obese";
        }

        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        trainCar[num].printCar();
        System.out.println("\nAverage mass index of all cats: "
                + (String.format("%.2f", average))
                + "\nIn average, the cats in the train are *"
                + categori + "*\n");
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int catTotal = input.nextInt();
        input.nextLine();
        TrainCar[] trainCar = new TrainCar[catTotal];
        for (int i = 0; i < catTotal; i++) {
            String cat = input.nextLine();
            String[] format = cat.split(",");
            if (trainCar[0] == null) {
                trainCar[0] = new TrainCar(new WildCat(format[0], Double.parseDouble(format[1]),
                        Double.parseDouble(format[2])));
            } else {
                trainCar[i] = new TrainCar(new WildCat(format[0], Double.parseDouble(format[1]),
                        Double.parseDouble(format[2])), trainCar[i - 1]);
            }
        }
        int departNum = trainToDepart(trainCar, 0, catTotal - 1);
        depart(trainCar, departNum, departNum + 1);

        // Jika masih ada TrainCar yang belum berangkat
        while (catTotal - (departNum + 1) != 0) {
            trainCar[departNum + 1].next = null;
            int departNumNow = trainToDepart(trainCar, departNum + 1, catTotal - 1);
            int catTotalNow = departNumNow - departNum; 
            depart(trainCar, departNumNow, catTotalNow);
            departNum = departNumNow; 
        }
    }
}
public class TrainCar {
    /* Atribut */
    public static final double EMPTY_WEIGHT = 20; // In kilograms
    TrainCar next;
    WildCat cat;

    /* Konstruktor */
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }
    
    /* Method menghitung total berat TrainCar dan isinya */
    public double computeTotalWeight() {
        if (next == null) {
            return (TrainCar.EMPTY_WEIGHT + this.cat.weight);
        } else {
            return (TrainCar.EMPTY_WEIGHT + this.cat.weight + next.computeTotalWeight());
        }
    }
    
    /* Method menghitung total BMI kucing */
    public double computeTotalMassIndex() {
        if (next == null) {
            return (this.cat.computeMassIndex());
        } else {
            return (this.cat.computeMassIndex() + next.computeTotalMassIndex()); 
        }
    }

    /* Method untuk print semua TrainCar yang berangkat */
    public void printCar() {
        if (next == null) {
            System.out.print("(" + this.cat.name + ")");
        } else {
            System.out.print("(" + this.cat.name + ")--");
            this.next.printCar(); 
        }
    }
}

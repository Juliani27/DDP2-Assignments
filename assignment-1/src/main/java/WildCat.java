public class WildCat {
    /* Atribut */
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    /* Konstruktor */
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    /* Method menghitung BMI Kucing*/
    public double computeMassIndex() {
        double bmi = this.weight / (this.length * this.length * 0.0001);
        return bmi;
    }
}

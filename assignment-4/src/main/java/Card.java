import javax.swing.JButton;

public class Card extends JButton {

    /*
    The status whether the card is selected or not by the user.
     */
    private boolean selected;

    /*
    The status of whether the card has been matched or not with it's pair.
     */
    private boolean matched;

    /*
    The type of the card.
     */
    private int type;

    /**
     * Constructor of Card Class.
     */
    public Card() {
        super();
        this.selected = false;
        this.matched = false;
    }

    /**
     * Return the card's type.
     * @return the card's type.
     */
    public int getType() {
        return this.type;
    }

    /**
     * Change the card's type.
     * @param type the card's type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Check if the type of two cards is same or not.
     * @param card another card
     * @return true if the type is same, false otherwise.
     */
    public boolean sameType(Card card) {
        return this.type == card.type;
    }

    /**
     * Return true if the card is selected by the user, false otherwise.
     * @return true if the card is selected by the user, false otherwise.
     */
    public boolean isSelected() {
        return this.selected;
    }

    /**
     * Change the card's selected.
     * @param selected boolean selected
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * Return true if the card has been matched with it's partner, false otherwise.
     * @return true if the card has been matched with it's partner, false otherwise.
     */
    public boolean isMatched() {
        return this.matched;
    }

    /**
     * Change the card's matched.
     * @param matched boolean matched
     */
    public void setMatched(boolean matched) {
        this.matched = matched;
    }
}
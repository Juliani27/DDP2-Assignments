import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.LineBorder;

public class Board extends JPanel implements ActionListener {

    /*
    The board's number of rows.
     */
    private int numberOfRows;

    /*
    The board's number of columns.
     */
    private int numberOfColumns;

    /*
    The number of cards to Match.
     */
    private int maxSelectedCards;

    /*
    Peek Delay.
     */
    private int peekDelay;

    /*
    Images number for every card's type.
     */
    private ArrayList<Integer> imgNum;

    /*
    The total of cards that have been matched.
     */
    private int matchedPairs;

    /*
    The number of tries.
     */
    private int attempts;

    /*
    The cards that have been selected by the user
     */
    private int selectedCards;

    /*
    Array for cards that will be checked whether they matched or not.
     */
    private Card[] cardChecker;

    /*
    Array for all cards.
     */
    private Card[][] cards;

    /*
    Timer to peek the cards.
     */
    private Timer timer;

    /**
     * Constructor of Board Class.
     * @param numberOfRows The board's number of rows
     * @param numberOfColumns The board's number of column
     * @param maxSelectedCards The number of cards to Match
     * @param peekDelay Peek Delay
     */
    public Board(int numberOfRows, int numberOfColumns, int maxSelectedCards, int peekDelay) {
        super();
        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numberOfColumns;
        this.maxSelectedCards = maxSelectedCards;
        this.peekDelay = peekDelay;
        this.imgNum = new ArrayList<>();
        this.cardChecker = new Card[this.maxSelectedCards];
        this.cards = new Card[this.numberOfRows][this.numberOfColumns];
        for (int row = 0; row < this.numberOfRows; row++) {
            for (int column = 0; column < this.numberOfColumns; column++) {
                this.cards[row][column] = new Card();
                this.cards[row][column].setBorder(new LineBorder(Color.BLACK, 3));
                this.cards[row][column].setBackground(Color.BLACK);
                this.cards[row][column].addActionListener(this);
                add(cards[row][column]);
            }
        }
        setBackground(Color.WHITE);
        setLayout(new GridLayout(this.numberOfRows, this.numberOfColumns));
        init();
    }

    /**
     * Setup before the game begin.
     */
    public void init() {
        reset();
        randomNum();
        setImages();
        peek();
    }

    /**
     * Return the number of attempts.
     * @return the number of attempts.
     */
    public int getAttempts() {
        return this.attempts;
    }

    /**
     * Return card with given row and column.
     * @param row the board's row.
     * @param column the board's column.
     * @return card with given row and column.
     */
    public Card getCards(int row, int column) {
        return this.cards[row][column];
    }

    /**
     * Resets the board.
     */
    private void reset() {
        this.matchedPairs = 0;
        this.attempts = 0;
        this.selectedCards = 0;
        for (int row = 0; row < this.numberOfRows; row++) {
            for (int column = 0; column < this.numberOfColumns; column++) {
                this.cards[row][column].setVisible(true);
                this.cards[row][column].setSelected(false);
                this.cards[row][column].setMatched(false);
            }
        }
    }

    /**
     * Sets the card images.
     */
    private void setImages() {
        Random rand = new Random();
        for (int row = 0; row < this.numberOfRows; row++) {
            for (int column = 0; column < this.numberOfColumns; column++) {
                int idx = rand.nextInt(this.imgNum.size());
                String img = ("images/" + this.imgNum.get(idx) + ".jpg");
                cards[row][column].setIcon(new ImageIcon(((new ImageIcon(img)).getImage())
                    .getScaledInstance(140, 140, java.awt.Image.SCALE_SMOOTH)));
                cards[row][column].setType(this.imgNum.get(idx));
                this.imgNum.remove(idx);
            }
        }
    }

    /**
     * Shuffles the image numbers.
     */
    private void randomNum() {
        for (int num = 0;
             num < (this.numberOfColumns * this.numberOfRows) / maxSelectedCards; num++) {
            for (int add = 0; add < maxSelectedCards; add++) {
                this.imgNum.add(num + 1);
            }
        }
        Collections.shuffle(this.imgNum);
    }

    /**
     * Shows card within the specified time (Peek Delay).
     */
    private void peek() {
        ActionListener showCard = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                cardImage();
            }
        };
        timer = new Timer(this.peekDelay, showCard);
        timer.setRepeats(false);
        timer.start();
    }

    /**
     * Shows the selected card's image.
     * @param card Selected Card.
     */
    private void showImage(Card card) {
        for (int row = 0; row < this.numberOfRows; row++) {
            for (int column = 0; column < this.numberOfColumns; column++) {
                if (cards[row][column] == card) {
                    String img = ("images/" + cards[row][column].getType() + ".jpg");
                    cards[row][column].setIcon(new ImageIcon(((new ImageIcon(img)).getImage())
                        .getScaledInstance(140, 140, java.awt.Image.SCALE_SMOOTH)));
                }
            }
        }

    }

    /**
     * Changes the card's image.
     */
    private void cardImage() {
        for (int row = 0; row < this.numberOfRows; row++) {
            for (int column = 0; column < this.numberOfColumns; column++) {
                if (!cards[row][column].isSelected()) {
                    if (cards[row][column].isMatched()) {
                        this.cards[row][column].setVisible(false);
                        this.cards[row][column].setType(19);
                    } else {
                        String img = ("images/19.jpg");
                        cards[row][column].setIcon(new ImageIcon(((new ImageIcon(img)).getImage())
                            .getScaledInstance(140, 140, java.awt.Image.SCALE_SMOOTH)));
                    }
                } else {
                    showImage(cards[row][column]);
                }
            }
        }
    }

    /**
     * Call checkCards method if the total of selected cards equals the maximum of selected cards.
     * @param card Selected card.
     */
    public void actionPerformed(ActionEvent card) {
        if (timer.isRunning()) {
            timer.stop();
            cardImage();
        }
        this.selectedCards++;
        if (this.selectedCards <= this.maxSelectedCards) {
            showImage((Card) card.getSource());
            cardChecker[this.selectedCards - 1] = (Card) card.getSource();
        }
        if (this.selectedCards == this.maxSelectedCards) {
            for (int idx = 0; idx < this.selectedCards; idx++) {
                for (int idx2 = idx + 1; idx2 < this.selectedCards; idx2++) {
                    if (cardChecker[idx].equals(cardChecker[idx2])) {
                        selectedCards--;
                        return;
                    }
                }
            }
            checkCards();
        }
    }

    /**
     * Checks if the selected cards type are the same.
     */
    private void checkCards() {
        boolean sameType = true;
        for (int idx = 0; idx < this.selectedCards; idx++) {
            cardChecker[idx].setSelected(false);
            for (int idx2 = idx + 1; idx2 < this.selectedCards; idx2++) {
                cardChecker[idx2].setSelected(false);
                if (!cardChecker[idx].sameType(cardChecker[idx2])) {
                    sameType = false;
                }
            }
        }
        for (int card = 0; card < this.selectedCards; card++) {
            if (sameType) {
                cardChecker[card].setMatched(true);
            } else {
                cardChecker[card].setMatched(false);
            }
        }
        showImage(cardChecker[this.selectedCards - 1]);
        peek();
        this.attempts++;
        this.selectedCards = 0;
        if (sameType) {
            this.matchedPairs += 1;
        }
    }

    /**
     * Checks if each card in Cards has been matched or not.
     * @return true if each card has been matched, false otherwise.
     */
    public boolean isSolved() {
        return this.matchedPairs == (this.numberOfRows * this.numberOfColumns) / maxSelectedCards;
    }
}
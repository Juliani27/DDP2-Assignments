import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Game extends JFrame {

    /*
    The board's number of rows.
     */
    private static int NUMBER_OF_ROWS = 6;

    /*
    The board's number of columns.
     */
    private static int NUMBER_OF_COLUMNS = 6;

    /*
    The number of cards to Match.
     */
    private static int MAX_SELECTED_CARDS = 2;

    /*
    Peek Delay.
     */
    private static int PEEK_DELAY = 1000;

    /*
    The game's board.
     */
    private Board board;

    /*
    Button to play a new game.
     */
    private JButton retryButton;

    /*
    Button to exit the Game.
     */
    private JButton exitButton;

    /*
    Label for attemps counter.
     */
    private JLabel counter;

    /*
    Background that will appear once the user finish the game.
     */
    private JPanel bg;

    /**
     * Constructor of Game Class.
     */
    public Game() {
        super("Javari Park: Match the Animals!");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setIconImage(new ImageIcon("images/19.jpg").getImage());

        //Congratulation Background
        bg = new JPanel();
        JLabel img = new JLabel(new ImageIcon("images/finish.jpg"));
        img.setLocation(0,0);
        bg.add(img);
        bg.setBackground(Color.PINK);

        //Board
        board = new Board(NUMBER_OF_ROWS, NUMBER_OF_COLUMNS, MAX_SELECTED_CARDS, PEEK_DELAY);
        for (int row = 0; row < NUMBER_OF_ROWS; row++) {
            for (int column = 0; column < NUMBER_OF_COLUMNS; column++) {
                board.getCards(row, column).addMouseListener(new MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        counter.setText("Number of tries : " + board.getAttempts());
                        finalMessage();
                    }
                });
            }
        }
        add(board, BorderLayout.WEST);

        //Other buttons Panel
        JPanel board2 = new JPanel();
        board2.setOpaque(true);
        board2.setBackground(Color.PINK);
        board2.setLayout(null);

        retryButton = new JButton("Play Again?");
        retryButton.setSize(150, 80);
        retryButton.setLocation(50, 300);
        retryButton.setBackground(Color.WHITE);
        retryButton.setForeground(Color.PINK);
        retryButton.setBorder(new LineBorder(Color.WHITE));
        retryButton.setFont(new Font("Comic Sans MS", Font.PLAIN, 23));
        retryButton.addMouseListener(btnMouseListener);
        board2.add(retryButton);

        exitButton = new JButton("Exit");
        exitButton.setSize(150, 80);
        exitButton.setLocation(50, 400);
        exitButton.setBackground(Color.WHITE);
        exitButton.setForeground(Color.PINK);
        exitButton.setBorder(new LineBorder(Color.WHITE));
        exitButton.setFont(new Font("Comic Sans MS", Font.PLAIN, 23));
        exitButton.addMouseListener(btnMouseListener);
        board2.add(exitButton);

        counter = new JLabel("Number of tries : 0");
        counter.setSize(200, 80);
        counter.setLocation(35, 480);
        counter.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
        board2.add(counter);
        add(board2, BorderLayout.CENTER);

        pack();
        setSize(1130, 840);
        setLocationByPlatform(true);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setResizable(false);
        setVisible(true);
    }

    /**
     * If retryButton is clicked, a new game will begin.
     * If exitButton is clicked, user will exit the game.
     */
    private MouseListener btnMouseListener = new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
            if (e.getComponent() == retryButton) {
                board.setVisible(true);
                board.init();
                counter.setText("Number of tries : 0");
            } else if (e.getComponent() == exitButton) {
                dispose();
            }
        }
    };

    /**
     * Shows the congratulation background once the user finish the game.
     */
    private void finalMessage() {
        if (board.isSolved()) {
            board.setVisible(false);
            add(bg, BorderLayout.WEST);
        }
    }

    public static void main(String[] args) {
        if ((NUMBER_OF_ROWS * NUMBER_OF_COLUMNS) % MAX_SELECTED_CARDS == 0) {
            new Game();
        } else {
            System.out.println("The number of cards doesn't meet the requirement");
        }
    }
}